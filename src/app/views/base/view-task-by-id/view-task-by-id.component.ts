import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TaskService} from '../../../services/task.service';

@Component({
  selector: 'app-view-task-by-id',
  templateUrl: './view-task-by-id.component.html',
  styleUrls: ['./view-task-by-id.component.scss']
})
export class ViewTaskByIdComponent implements OnInit {

  task: any;
  subTask = false;
  categories = false;

  constructor(private route: ActivatedRoute,
              private taskService: TaskService) {
  }

  ngOnInit(): void {
    // First get the product id from the current route.
    const routeParams = this.route.snapshot.paramMap;
    const taskIdFromRoute = Number(routeParams.get('id'));

    this.taskService.viewTaskById(taskIdFromRoute).subscribe(res => {
      console.log(res);
      this.task = res.task[0];
      if (typeof this.task.sub_tasks !== 'undefined' && this.task.sub_tasks.length > 0) {
        this.subTask = true;
      }
      if (typeof this.task.categories !== 'undefined' && this.task.categories.length > 0) {
        this.categories = true;
      }
    });
  }

}
