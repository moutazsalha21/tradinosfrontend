import {Component, OnInit} from '@angular/core';
import {TaskService} from '../../../services/task.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-view-all-task',
  templateUrl: './view-all-task.component.html',
  styleUrls: ['./view-all-task.component.scss']
})
export class ViewAllTaskComponent implements OnInit {

  public tasks: any;
  public isAdmin = false;
  public endFlag = '';
  public deadLine = '';

  constructor(private taskService: TaskService,
              private router: Router) {
  }

  ngOnInit(): void {
    if (localStorage.getItem('role') === 'Admin') {
      this.isAdmin = true;
    }
    this.taskService.getAllTasks().subscribe((result: any) => {
      console.log(result);
      this.tasks = result.tasks;
    });
  }

  deleteById(id: number) {
    this.taskService.deleteTaskById(id).subscribe((result: any) => {
      console.log(result);
      if (result.status === 200) {
        this.tasks.filter(task => {
          if (task.id === id) {
            this.tasks.splice(this.tasks.indexOf(task), 1);
          }
        });
      } else {
        alert('Unauthorized');
      }
    });
  }

  viewById(id: number) {
    this.router.navigate(['base/view-task-ById', id]);
  }

  Search() {
    const searchTasks = {
      endFlag: this.endFlag,
      deadLine: this.deadLine
    };
    console.log(searchTasks);
    this.taskService.searchTasks(searchTasks).subscribe((response: any) => {
      console.log(response);
      this.tasks = response.tasks;
    });
  }

}
