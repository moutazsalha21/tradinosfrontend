import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TaskService} from '../../../services/task.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.scss']
})
export class CreateTaskComponent implements OnInit {

  public createtask: FormGroup;
  submitted = false;
  users: any = [];
  categories: any = [];
  selected: any = [];

  constructor(private formBuilder: FormBuilder,
              private taskService: TaskService,
              private router: Router) {
    this.createtask = this.formBuilder.group({
      description: ['', Validators.required],
      deadLine: ['', Validators.required],
      endFlag: ['', Validators.required],
      userAssign: ['', Validators.required],
      subTask: this.formBuilder.array([]),
      categoryAssign: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    if (localStorage.getItem('role') !== 'Admin') {
      this.router.navigate(['login']);
    }
    this.taskService.getAllUser().subscribe((result: any) => {
      console.log(result.users);
      if (result.status === 200) {
        this.users = result.users;
      } else {
        alert('Unauthorized');
      }
    });

    this.taskService.getAllCategories().subscribe((result: any) => {
      console.log(result.categories);
      if (result.status === 200) {
        this.categories = result.categories;
      } else {
        alert('Unauthorized');
      }
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.createtask.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.createtask.invalid) {
      return;
    }

    console.log(this.createtask.value);

    this.taskService.createTask(this.createtask.value).subscribe((result: any) => {
      console.log(result);
      if (result.status === 200) {
        this.router.navigate(['/base/view-all-task']);
      } else {
        alert('Unauthorized');
      }
    });
  }

  onReset() {
    this.submitted = false;
    this.createtask.reset();
  }

  subTask(): FormArray {
    return this.createtask.get('subTask') as FormArray;
  }

  newSubTask(): FormGroup {
    return this.formBuilder.group({
      sub: '',
    });
  }

  addSubTask() {
    this.subTask().push(this.newSubTask());
  }

  removeSubTask(i: number) {
    this.subTask().removeAt(i);
  }


}
