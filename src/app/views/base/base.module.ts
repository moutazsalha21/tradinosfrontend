// Angular
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';

// Forms Component
import {FormsComponent} from './forms.component';

// Components Routing
import {BaseRoutingModule} from './base-routing.module';
import {ViewAllTaskComponent} from './view-all-task/view-all-task.component';
import {ViewTaskByIdComponent} from './view-task-by-id/view-task-by-id.component';
import {CreateTaskComponent} from './create-task/create-task.component';
import {NgSelectModule} from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BaseRoutingModule,
    ReactiveFormsModule,
    NgSelectModule
  ],
  declarations: [
    FormsComponent,
    ViewAllTaskComponent,
    ViewTaskByIdComponent,
    CreateTaskComponent
  ]
})
export class BaseModule {
}
