import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent {
  loginFrom: FormGroup;
  submitted = false;
  errorAfterSubmit = false;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) {
    this.loginFrom = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginFrom.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginFrom.invalid) {
      return;
    }

    const loginObj = {
      email: this.f.email.value,
      password: this.f.password.value
    };

    this.authService.login(loginObj).subscribe((res: any) => {
      console.log(res);
      if (res.status === 200) {
        localStorage.setItem('token', res.token);
        localStorage.setItem('role', res.role);
        this.router.navigate(['base/view-all-task']);
      } else {
        alert('invalid email or password please try again');
      }
    });
  }

  onReset() {
    this.submitted = false;
    this.loginFrom.reset();
  }

  register() {
    this.router.navigate(['register']);
  }
}
