import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {
  }

  login(loginObj): Observable<any> {
    return this.http.post<any>('http://127.0.0.1:8000/api/login', loginObj);
  }

  register(registerObj): Observable<any> {
    return this.http.post<any>('http://127.0.0.1:8000/api/register', registerObj);
  }

  getToken() {
    return localStorage.getItem('token');
  }
}
