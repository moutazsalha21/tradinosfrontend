import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FormsComponent} from './forms.component';
import {ViewAllTaskComponent} from './view-all-task/view-all-task.component';
import {ViewTaskByIdComponent} from './view-task-by-id/view-task-by-id.component';
import {CreateTaskComponent} from './create-task/create-task.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Base'
    },
    children: [
      {
        path: '',
        redirectTo: 'view-all-task'
      },
      {
        path: 'view-all-task',
        component: ViewAllTaskComponent
      },
      {
        path: 'view-task-ById/:id',
        component: ViewTaskByIdComponent
      },
      {
        path: 'create-task',
        component: CreateTaskComponent
      },
      {
        path: 'forms',
        component: FormsComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BaseRoutingModule {
}
