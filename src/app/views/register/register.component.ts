import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html'
})
export class RegisterComponent {

  registerFrom: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) {
    this.registerFrom = this.formBuilder.group({
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerFrom.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerFrom.invalid) {
      return;
    }

    const registerObj = {
      name: this.f.username.value,
      email: this.f.email.value,
      password: this.f.password.value
    };

    this.authService.register(registerObj).subscribe((res: any) => {
      console.log(res);
      if (res.status === 200) {
        this.router.navigate(['login']);
      } else {
        alert('error in register process please try again');
      }
    });
  }

}
