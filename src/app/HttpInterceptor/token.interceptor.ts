import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthService} from '../services/auth.service';
import {catchError} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService,
              private router: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    request = request.clone({
      setHeaders: {
        Authorization: `Bearer ${this.authService.getToken()}`
      }
    });

    return next.handle(request).pipe(catchError((err: any) => {
      console.log('error in login');
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) {
          console.log('Unauthorized');
          this.router.navigate(['login']);
        } else if (err.status === 404) {
          this.router.navigate(['404']);
        }
      }
      return new Observable<HttpEvent<any>>();
    }));
  }
}
