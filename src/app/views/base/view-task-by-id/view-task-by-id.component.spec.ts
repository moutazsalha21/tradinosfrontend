import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTaskByIdComponent } from './view-task-by-id.component';

describe('ViewTaskByIdComponent', () => {
  let component: ViewTaskByIdComponent;
  let fixture: ComponentFixture<ViewTaskByIdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewTaskByIdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTaskByIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
