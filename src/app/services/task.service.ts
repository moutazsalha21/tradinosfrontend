import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private http: HttpClient) {
  }

  getAllTasks(): Observable<any> {
    return this.http.get('http://127.0.0.1:8000/api/tasks');
  }

  deleteTaskById(id: number): Observable<any> {
    return this.http.delete('http://127.0.0.1:8000/api/tasks/' + id);
  }

  viewTaskById(id: number): Observable<any> {
    return this.http.get('http://127.0.0.1:8000/api/tasks/' + id);
  }

  searchTasks(searchTasks: any): Observable<any> {
    const params = new HttpParams()
      .set('endFlag', searchTasks.endFlag)
      .set('deadLine', searchTasks.deadLine);
    return this.http.get('http://127.0.0.1:8000/api/search/tasks', {params});
  }

  createTask(taskObj): Observable<any> {
    return this.http.post('http://127.0.0.1:8000/api/tasks', taskObj);
  }

  getAllUser(): Observable<any> {
    return this.http.get('http://127.0.0.1:8000/api/users');
  }

  getAllCategories(): Observable<any> {
    return this.http.get('http://127.0.0.1:8000/api/categories');
  }
}
